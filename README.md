# nixos-config

## Set up new machine

* Clone this repo to `/etc/nixos`
* Copy and edit configurations as required:
  * `cp configurations/spitfire.nix configurations/$NEWMACHINE.nix`
  * `cp machines/spitfire.nix machines/$NEWMACHINE.nix`
* Move generated hardware-configuration.nix to correct dir:
  * `mv hardware-configuration.nix hardware-configurations/$NEWMACHINE.nix`
* Create symlink to current machine configuration:
  * `ln -sf configurations/$NEWMACHINE.nix configuration.nix`
* Update config as per normal: `sudo nixos-rebuild switch --upgrade`

Note: The configuration.nix symlink is in gitignore as it will point to a different config on each machine and should not be checked in

## TODO 

* Reorganise modules - Conditional installs
  * Eg. If xorg, install firefox... etc
  * (https://github.com/a-schaefers/nix-config)
  * (https://github.com/cleverca22/nixos-configs)

Started from ghuntley's example repo.  See below

# dotfiles-nixos
source drop as reference for others on how to setup nixos and share configs between multiple machines. If you find this helpful then you may also like [my notes on installing NixOS](http://ghuntley.com/notes/hetzner) on a Hetzner bare metal server with ZFS encryption (via LUKS) manually without any form of automation (ie nixops or terraform).


<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Today is the 4th month of running <a href="https://twitter.com/nixos_org?ref_src=twsrc%5Etfw">@nixos_org</a>. I&#39;m never going back to ubuntu/debian/whatever ever. Nix is declarative, immutable and the way it handles state is genius. It&#39;s the perfect distro for the desktop/servers. To celebrate I&#39;ve released my cfgs <a href="https://t.co/WiGsLQULtY">https://t.co/WiGsLQULtY</a> <a href="https://t.co/tGCguvLMwR">pic.twitter.com/tGCguvLMwR</a></p>&mdash; 𝓖𝓮𝓸𝓯𝓯𝓻𝓮𝔂 𝓗𝓾𝓷𝓽𝓵𝓮𝔂 (@GeoffreyHuntley) <a href="https://twitter.com/GeoffreyHuntley/status/985432389097668608?ref_src=twsrc%5Etfw">April 15, 2018</a></blockquote>
