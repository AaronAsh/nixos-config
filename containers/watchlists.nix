{ config, pkgs, ... }:

let hostAddr = "192.168.100.1";
in
{ containers.watchlistsdb =
  {
    privateNetwork = true;
    hostAddress = hostAddr;
    localAddress = "192.168.100.2";
    config =
    { config, pkgs, ... }:
    { services.postgresql.enable = true;
      services.postgresql.package = pkgs.postgresql96;
    };
  };

  containers.watchlistsweb =
  {
    privateNetwork = true;
    hostAddress = hostAddr;
    localAddress = "192.168.100.3";
    config =
    { config, pkgs, ... }:
    {
    };
  };
  networking.networkmanager.unmanaged = [ "interface-name:ve-*" ];
}
