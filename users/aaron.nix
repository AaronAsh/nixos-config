{ config, lib, pkgs, ... }:
with lib;

let secrets = import ../secrets.nix;
in
{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.aaron = {
    description = "aaron";
    group = "aaron";
    extraGroups = [
      "audio"
      "libvirtd"
      "networkmanager"
      "sway"
      "users"
      "vboxusers"
      "video"
      "wheel"
    ];

    uid = 1000;

    createHome = true;
    home = "/home/aaron";
    shell = "/run/current-system/sw/bin/bash";

    # AA TODO
    # openssh.authorizedKeys.keys = secrets.sshKeys.yeah;

  };
  users.extraGroups.aaron.gid = 1000;

  system.activationScripts =
  {
    # Configure various dotfiles.
    dotfiles = stringAfter [ "users" ]
    ''
      # git clone https://github.com/ashyisme/dotrc /home/aaron/dotrc
      # cd /home/aaron/dotrc

      # AA TODO git clone dotfiles and run gnu stow

    '';
  };
}
