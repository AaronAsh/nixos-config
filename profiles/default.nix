{ config, pkgs, ... }:

{

  imports = [
    ../pkgs/bash/config.nix
    ../pkgs/emacs/config.nix
    ../pkgs/vim/config.nix
  ];

  # List packages installed in system profile. To search by name, run:
  # nix-env -qaP | grep wget
  # AA 20180715 Commented out the stuff I haven't used
  environment.systemPackages = with pkgs; [
    ack
    ag
    # apg # random password generation
    aspell
    aspellDicts.en
    bind  # nslookup, dig
    # blackbox # safely store secrets in git
    bridge-utils
    ctags
    curl
    dos2unix
    dstat
    # duplicity
    elinks
    expect
    file
    fzf
    #unstable.git-lfs
    #unstable.gitAndTools.gitFull
    git
    gitAndTools.gh
    gnumake
    gnupg
    htop
    #idutils # AA 20200315 broken on nixos-unstable
    iftop
    iotop
    lsof
    mkpasswd
    mosh
    # most
    ncftp
    ncdu
    ncurses
    nix-prefetch-scripts
    nmap
    nox # https://github.com/madjar/nox
    openssl
    #p7zip # p7zip is abandoned apparently - security risk
    pciutils
    pmutils
    psmisc
    ranger
    stdenv
    stow
    strace
    sudo
    sysstat
    tcpdump
    tmux
    # tmuxinator
    # unison
    unzip
    usbutils
    vim_configurable
    w3m
    wget
    youtube-dl
    zip
    # zsh
  ];

  # custom packages
  #nixpkgs.config.packageOverrides = pkgs: rec {
  #  heirloom-mailx = pkgs.callPackage ../pkgs/heirloom-mailx/default.nix { };
  #  unison_2_40_102 = pkgs.callPackage ../pkgs/unison/unison-2.40.102.nix { lablgtk = pkgs.ocamlPackages.lablgtk; };
  #};

  # Enable zsh as a login shell
  # programs.zsh.enable = true;
}
