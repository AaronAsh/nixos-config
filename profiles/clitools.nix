{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    ripgrep
    fd
    #gotop # replaced with ytop
    #ytop
    unstable.neovim
  ];
}
