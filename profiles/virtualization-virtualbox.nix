{ config, pkgs, ... }:

{
  nixpkgs.config = {
  };

  environment.systemPackages = with pkgs; [
    vagrant
  ];

  virtualisation.virtualbox = {
    host.enable = true;
  };

}
