{ config, pkgs, lib, ... }:

{
  environment.systemPackages = with pkgs; [
    (pkgs.dwarf-fortress-packages.dwarf-fortress-full.override {
       #dfVersion = "0.44.11";
       #theme = "mayday";
       #theme = "spacefox";
       enableIntro = false;
       enableDFHack = true;
       #enableFPS = true;
    })
  ];

}
