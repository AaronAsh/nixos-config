{ config, pkgs, ... }:

{
  nixpkgs.config = {
  };

  environment.systemPackages = with pkgs; [
    kvm
    qemu

    # vagrant in a way that allows `vagrant plugin install vagrant-libvirt` to succeed
    libxml2
    libvirt
    zlib
    ruby.devEnv
    pkgconfig
    vagrant
    #(import <nixpkgs> {
	#overlays = [
	  #(self: super: {
	    #bundler = super.bundler.overrideAttrs (old: {
	      #name = "bundler-1.16.1";
	      #src = super.fetchurl {
		#url = "https://rubygems.org/gems/bundler-1.16.1.gem";
		#sha256 = "1s2nq4qnffxg3kwrk7cnwxcvfihlhxm9absl2l6d3qckf3sy1f22";
	      #};
	    #});
	  #})
	#];
      #}).vagrant
  ];

  virtualisation.libvirtd = {
    enable = true;
    onShutdown = "shutdown";
  };
  networking.firewall.checkReversePath = false;

}
