{ config, lib, pkgs, ... }:

{
  #imports = [
    #./haskell.nix
  #];

  # install development packages
  environment.systemPackages = with pkgs; [
    #zlib
    jetbrains.datagrip
    #jetbrains.pycharm-professional
    dbeaver

    rstudioWrapper

    config.boot.kernelPackages.bcc

    nixfmt
  ];
  # microsoft odbc driver.  Does this set up /etc/odbcinst.ini too?
  environment.unixODBCDrivers = [ pkgs.unixODBCDrivers.msodbcsql17 ];

  # custom packages
  nixpkgs.config.packageOverrides = pkgs: rec {
  };

}
