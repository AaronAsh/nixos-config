{ config, lib, pkgs, ... }:

{

  programs.sway.enable = true;
  programs.light.enable = true;

  services.xserver = {
    enable = true;
    enableCtrlAltBackspace = true;

    libinput.enable = true;
    libinput.naturalScrolling = false;
    libinput.middleEmulation = true;
    libinput.tapping = true;

    windowManager = {
      i3.enable = true;
      i3.package = pkgs.i3-gaps;
      #default = "i3";
    };

    # AA 20200508 gnome screenlock is not working for some reason atm. Swap to plasma temporarily
    displayManager = {
      gdm.enable = true;
      gdm.wayland = true;
      #sessionCommands = "i3status &";
      #defaultSession = "none+i3";
    };

    #displayManager.sddm.enable = true;
    #desktopManager.plasma5.enable = true;

    desktopManager = {
      # default = "none";
      # AA 20200508 gnome screenlock is not working for some reason atm. Swap to plasma temporarily
      gnome3.enable = true;
      xterm.enable = false;
    };

  };

  # programs.firejail = {
  #   enable = true;
  #   wrappedBinaries = {
  #     chromium = "${lib.getBin pkgs.chromium}/bin/chromium";
  #     firefox = "${lib.getBin pkgs.firefox}/bin/firefox";
  #     mpv = "${lib.getBin pkgs.mpv}/bin/mpv";
  #   };
  # };


  #nixpkgs.config = {
    #chromium = {
      #jre = false;
      #enableGoogleTalkPlugin = true;
      #enableAdobeFlash = false;
      #enablePepperPDF = true;
    #};

    #firefox = {
      #jre = false;
      #enableGoogleTalkPlugin = true;
      #enableAdobeFlash = false;
      #enablePepperPDF = true;
    #};
  #};

  #fonts.fontDir.enable = true;
  fonts = {
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      corefonts
      dejavu_fonts
      #emojione
      fira-code
      font-awesome-ttf
      inconsolata
      ibm-plex
      proggyfonts
      powerline-fonts
      source-code-pro
      source-sans-pro
      source-serif-pro
      terminus_font
      ubuntu_font_family
      vistafonts

      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
    ];
  };

  nixpkgs.config.packageOverrides = pkgs: {
      redshift = pkgs.redshift.overrideAttrs (oldAttrs: rec {
      #src = pkgs.fetchFromGitHub {
	#owner = "breznak";
	#repo = "redshift";
	#rev = "Lourens-Rich-master";
	#sha256 = "1b9gjz1crw8fzlpdisafn6lx37m5dg7s1ak35qcrgzgqlcsldw1l";
      #};
      src = pkgs.fetchFromGitHub {
	owner = "minus7";
	repo = "redshift";
	rev = "wayland";
	sha256 = "0nbkcw3avmzjg1jr1g9yfpm80kzisy55idl09b6wvzv2sz27n957";
      };
    });
    unstable = import <nixos-unstable> {
      # pass the nixpkgs config to the unstable alias
      # to ensure `allowUnfree = true;` is propagated:
      config = config.nixpkgs.config;
    };
  };

  environment.systemPackages = with pkgs; [
    xorg.xrdb
    xorg.setxkbmap
    xorg.iceauth # required for KDE applications (it's called by dcopserver)
    xorg.xlsclients
    xorg.xset
    xorg.xsetroot
    xorg.xinput
    xorg.xprop
    xorg.xauth
    xorg.xmodmap
    xorg.xbacklight
    numlockx
    xautolock
    xss-lock
    xtitle
    xclip
    xcape

    imagemagick

    xterm
    xdg_utils

    gparted

    # wayland stuff
    sway
    redshift
    slurp
    grim
    mako

    rofi
    dmenu
    feh       # for background image
    quota
    i3-gaps
    i3lock    # screen lock
    i3status  # sys info
    brightnessctl
    dunst     # desktop notifications
    libnotify # desktop notifications client
    scrot     # for screenshot
    pamixer
    playerctl

    terminator
    #alacritty

    chromium
    firefox
    qutebrowser
    epdfview
    wireshark
    psensor
    xfontsel
    slack
    #unstable.teams
    teams

    #unstable.paraview

    spotify
    vimHugeX
    i3minator
    evince
    libreoffice
    vlc
    mpv

    keybase
    keybase-gui

    # syncthing - Done as a service
    syncthing-gtk

    remmina

    #gnome3.nautilus
    #gnome3.eog
    #gnome3.gnome-screensaver

    vscode
    unstable.commitizen
  ];


  # Enable corsair keyboard/mouse
  # hardware.ckb.enable = true;

  # Enable 3D acceleration for 32bit applications (e.g. wine)
  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.driSupport = true;
  hardware.opengl.enable = true;

  # Allow ChromeCast to send/receive packets
  # http://askubuntu.com/a/326224/177448
  # networking.firewall.extraCommands = ''
  #   iptables -I INPUT -p udp -m udp --dport 32768:60999 -j ACCEPT
  # '';
}
