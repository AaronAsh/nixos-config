{ config, pkgs, ... }:

{
  nixpkgs.config = {
  };

  environment.systemPackages = with pkgs; [
    nixops
    virtmanager
  ];

  # Hypervisors moved to own file

  # moved to docker.nix
  #virtualisation.docker.enable = true;
  #users.users.aaron.extraGroups = [ "docker" ];
}
