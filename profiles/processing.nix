{ config, pkgs, lib, ... }:

{
  environment.systemPackages = with pkgs; [
    jdk11
    processing
  ];

}
