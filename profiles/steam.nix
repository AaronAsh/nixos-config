{ config, lib, pkgs, ... }:

{

  nixpkgs.config = {
    allowUnfree = true;

    packageOverrides = pkgs: {
      unstable = import <nixos-unstable> {
	# pass the nixpkgs config to the unstable alias
	# to ensure `allowUnfree = true;` is propagated:
	config = config.nixpkgs.config;
      };
    };
  };

  # Allow the Steam controller to be configured by Steam
  # services.udev.extraRules =
  #   ''
  #     # This rule is needed for basic functionality of the controller in Steam and keyboard/mouse emulation
  #     SUBSYSTEM=="usb", ATTRS{idVendor}=="28de", MODE="0666"

  #     # This rule is necessary for gamepad emulation
  #     KERNEL=="uinput", MODE="0660", GROUP="ghuntley", OPTIONS+="static_node=uinput"
  #   '';
  hardware.opengl.driSupport32Bit = true;
  hardware.pulseaudio.support32Bit = true;
  hardware.steam-hardware.enable = true;
  environment.systemPackages = with pkgs; [
    unstable.steam
    unstable.steam-run-native
  ];
}

