{ config, lib, pkgs, ... }:

{
  virtualisation.libvirtd.enable = true;
  virtualisation.lxc.enable = true;
  virtualisation.lxc.usernetConfig = ''
    bfo veth lxcbr0 10
  '';
  virtualisation.docker.enable = true;
  #virtualisation.docker.enableOnBoot = false; # Attempt to stop docker.service slowing down boot by 1.5minutes
  virtualisation.docker.storageDriver = "overlay";
  users.users.aaron.extraGroups = [ "docker" ];
}
