# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ../hardware-configurations/dellp3520.nix

      ../configuration-common.nix
      ../machines/dellp3520.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.supportedFilesystems = [ "ntfs" ];

  # networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;

  networking.wireguard.interfaces = {
    wg0 = {
      ips = [ "192.168.169.21/24" ];
      privateKeyFile = "/home/aaron/wireguard-keys/private";
      peers = [
        {
          publicKey = "VI5Ga7La/LCYGqjZ49rPjZzkJ6spgWXV5j2lB2cvE3o=";
          #allowedIPs = [ "0.0.0.0/0" ];  # Forward all traffic via VPN
          allowedIPs = [ "192.168.169.0/24" ];  # Forward only some subnets
          endpoint = "13.239.152.249:51820";

          persistentKeepalive = 25;
        }
      ];
    };
  };

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   wget vim
  # ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
  # services.xserver.enable = true;
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  #services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  services.xserver.displayManager.gdm.wayland = true;
  services.xserver.desktopManager.gnome3.enable = true;

  #services.xserver.videoDrivers = [ "intel" "nvidia" ];
  services.xserver.videoDrivers = [ "nvidia" ];
  #hardware.nvidia.prime = { # nixos-unstable
  hardware.nvidia.optimus_prime = { # nixos-20.03
    offload.enable = true;
    # Documentation: https://nixos.wiki/wiki/Nvidia#Optimus
    # Then you need to setup the Bus ID's of the cards as seen below.
    # Note: Bus ID is important and needs to be formatted properly
    # The Nvidia driver expects the bus ID to be in decimal format; However, lspci shows the bus IDs in hexadecimal format.
    # You can convert the value by
    #     Stripping any leading zeros from the bus numbers or if the number is above 09, convert it to decimal and use that value.
    #     Replacing any full stops with colons.
    #     Prefix the final value with "PCI".
    #"00:02.0 VGA compatible controller: Intel Corporation HD Graphics P630 (rev 04)"
    #"01:00.0 3D controller: NVIDIA Corporation GM107GLM [Quadro M620 Mobile] (rev a2)"
    nvidiaBusId = "PCI:1:0:0";
    intelBusId = "PCI:0:2:0";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.users.jane = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  # };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

}

