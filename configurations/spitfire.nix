# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ../hardware-configurations/spitfire.nix

      ../configuration-common.nix
      ../machines/spitfire.nix
    ];


  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.enableUnstable = true;


  services.zfs.autoScrub.enable = true;

  # Dual boot with win10 and win10 writes local time to the hardware clock (*censored*)
  time.hardwareClockInLocalTime = true;


  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  # system.stateVersion = "18.03"; # Did you read the comment?
  system.stateVersion = "18.09";
  #system.stateVersion = "19.09"; # Did you read the comment?

}
