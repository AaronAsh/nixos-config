
# Join the wifi:
nmtui

# Add the zfs filesystem to the install environment:
vim /etc/nixos/configuration.nix

## ---8<-------------------------8<---
  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.enableUnstable = true;
## ---8<-------------------------8<---

nixos-rebuild switch

# Load the just installed ZFS kernel module
modprobe zfs

# Create boot partition and (zfs) data partition
# See: https://github.com/zfsonlinux/pkg-zfs/wiki/HOWTO-install-Ubuntu-to-a-Native-ZFS-Root-Filesystem#step-2-disk-partitioning

# BIOS
# # 1024MB boot partition and 2nd partition to fill disk
# sfdisk /dev/sdX << EOF
# ,1GiB
# ;
# EOF

# UEFI
fdisk /dev/sdX
> n
> <CR>  #Enter for default (1)
> +512M
> t     # Change partition type
> <CR>  # Enter for default (1)
> 1     # UEFI type
> n
> 2
> <CR>  # Enter for default
> <CR>  # Rest of the space
> a     # Make it bootable
> w


## Use "-o ashift=12" to create your ZFS pool with 4K sectors
#zpool create -o ashift=12 -o altroot=/mnt rpool /dev/sdX2

# Encrypted zpool
zpool create -f -o ashift=12 -o altroot="/mnt" -O encryption=aes-256-gcm -O keyformat=passphrase zroot /dev/sdX2
zfs create -o mountpoint=none zroot/root
zfs create -o mountpoint=legacy -o sync=disabled zroot/root/tmp
zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=true zroot/root/home
zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=true zroot/root/nixos
mount -t zfs zroot/root/nixos /mnt
mkdir /mnt/{home,tmp,boot}
# assuming that /dev/sdX1 is the boot partition
mkfs.vfat /dev/sdX1
# mkfs.ext4 -m 0 -L boot -j /dev/sdX1
mount /dev/sdX1 /mnt/boot/
mount -t zfs zroot/root/home /mnt/home/
mount -t zfs zroot/root/tmp /mnt/tmp/

# Generate the NixOS configuration, as per the NixOS manual
nixos-generate-config --root /mnt

# Now edit the generated hardware config:
vim /mnt/etc/nixos/hardware-configuration.nix

## ---8<-------------------------8<---
# This is what you want:

  fileSystems."/" =
    { device = "zroot/root/nixos";
      fsType = "zfs";
    };

  fileSystems."/home" =
    { device = "zroot/root/home";
      fsType = "zfs";
    };

  fileSystems."/boot" =
    { device = "/dev/sdX1";
      fsType = "ext4";
    };
## ---8<-------------------------8<---

# configuration.nix needs an adjustment:
vim /mnt/etc/nixos/configuration.nix
# OR copy this whole repo to /mnt/etc/nixos/

## ---8<-------------------------8<---
# This is some more of what you want:

  boot.loader.grub.devices = [ "/dev/sdX" ];
  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.enableUnstable = true;
## ---8<-------------------------8<---

# Ready to go!
nixos-install
