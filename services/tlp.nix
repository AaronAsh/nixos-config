{ config, ... }:

{
  services.tlp = {
    enable = true;
    extraConfig = ''
CPU_SCALING_GOVERNOR_ON_AC=performance
CPU_SCALING_GOVERNOR_ON_BAT=powersave

    '';
  };
}
