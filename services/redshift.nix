{ config, pkgs, ... }:

{
  services.redshift = {
    enable = true;
    location.latitude = "-33.868820";
    location.longitude = "151.209290";
    temperature.night = 4000;
    brightness.night = "0.8";
  };
}
