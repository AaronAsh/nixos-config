{ config, pkgs, ... }:

{
  imports = [
    # ./mosh.nix
    # ./tinc.nix
    ./fail2ban.nix
    ./ntp.nix
    ./sshd.nix
  ];
}
