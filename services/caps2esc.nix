{ config, pkgs, ... }:

{
  # AA TODO: Currently this does the correct thing for the Capslock key
  # but *also* turns the Esc key into Capslock.
  # Find out how to get it to leave the Esc key alone.
  services.interception-tools.enable = true;  # caps2esc

}
