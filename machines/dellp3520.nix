{ config, pkgs, ... }:

{
  imports =
    [
      # ../profiles/android.nix
      ../profiles/audio.nix
      ../profiles/bluetooth.nix
      ../profiles/clitools.nix
      ../profiles/desktop.nix
      #../profiles/desktop-wayland-overlay.nix
      ../profiles/development.nix
      #../profiles/docker.nix
      # ../profiles/dotnet.nix
      # ../profiles/drawing.nix
      # ../profiles/email.nix
      # ../profiles/games.nix
      # ../profiles/dwarf-fortress.nix
      # ../profiles/graphics.nix
      # ../profiles/homeautomation.nix
      # ../profiles/latex.nix
      # ../profiles/livecoding.nix
      # ../profiles/maclaptop.nix
      ../profiles/mathematics.nix
      ../profiles/networking.nix
      #../profiles/powermanagement.nix
      # ../profiles/printing.nix
      #../services/redshift.nix
      ../services/tlp.nix
      #../services/fwupd.nix
      ../services/caps2esc.nix
      # ../profiles/rust.nix
      # ../profiles/ruby.nix
      # ../profiles/steam.nix
      ../profiles/virtualization-common.nix
      ../profiles/virtualization-libvirtd.nix
      #../profiles/virtualization-virtualbox.nix

      # Experimental
      # ../containers/watchlists.nix
    ];

  nixpkgs.config.allowUnfree = true;

#  networking.firewall.enable = false;
#  networking.firewall.allowPing = true;
# Set a static IP when I didn't want to set up dhcp
#  networking.defaultGateway = "192.168.0.1";
#  networking.interfaces.enp3s0 = {
#    ipAddress = "192.168.0.42";
#    prefixLength = 24;
#  };

  # Speed up development at the cost of possible build race conditions
  nix.buildCores = 8;

  zramSwap = {
    enable = true;
    memoryPercent = 20;
    #numDevices = 1;
    priority = 10;
  };

  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;

  # # Use the GRUB 2 boot loader.
  # boot.loader.grub.enable = true;
  # boot.loader.grub.version = 2;
  # boot.loader.grub.devices = [ "/dev/nvme0n1" ];

  # boot.initrd.luks.devices = [
  #   {
  #     name = "root"; device = "/dev/sda2"; preLVM = true;
  #   }
  # ];

  # boot.extraModprobeConfig = ''
  # options resume=/dev/rootvg/swap
  # '';

  #boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelPackages = pkgs.linuxPackages;

  networking.hostName = "dellp3520";
  networking.hostId = "fa3ffb36";

  networking.networkmanager.enable = true;

  hardware.bluetooth.enable = true;

  hardware.opengl.extraPackages = [ pkgs.vaapiIntel ];

}

