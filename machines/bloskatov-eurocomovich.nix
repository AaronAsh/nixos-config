{ config, pkgs, ... }:

{
  imports =
    [
      # ../profiles/android.nix
      ../profiles/audio.nix
      #../profiles/bluetooth.nix
      ../profiles/desktop.nix
      # ../profiles/development.nix
      #../profiles/docker.nix
      # ../profiles/dotnet.nix
      # ../profiles/drawing.nix
      ../profiles/email.nix
      ../profiles/games.nix
      # ../profiles/graphics.nix
      # ../profiles/homeautomation.nix
      # ../profiles/latex.nix
      # ../profiles/livecoding.nix
      # ../profiles/maclaptop.nix
      ../profiles/mathematics.nix
      ../profiles/networking.nix
      # ../profiles/powermanagement.nix
      # ../profiles/printing.nix
      ../services/redshift.nix
      # ../profiles/rust.nix
      # ../profiles/ruby.nix
      ../profiles/steam.nix
      ../profiles/virtualization-common.nix

      # Experimental
      ../containers/watchlists.nix
    ];

  nixpkgs.config.allowUnfree = true;

#  networking.firewall.enable = false;
#  networking.firewall.allowPing = true;
# Set a static IP when I didn't want to set up dhcp
#  networking.defaultGateway = "192.168.0.1";
#  networking.interfaces.enp3s0 = {
#    ipAddress = "192.168.0.42";
#    prefixLength = 24;
#  };

  # Speed up development at the cost of possible build race conditions
  nix.buildCores = 4;

  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.devices = [ "/dev/sda" ];

  # boot.initrd.luks.devices = [
  #   {
  #     name = "root"; device = "/dev/sda2"; preLVM = true;
  #   }
  # ];

  # boot.extraModprobeConfig = ''
  # options resume=/dev/rootvg/swap
  # '';


  networking.hostName = "bloskatov-eurocomovich";
  networking.hostId = "ae361947";

  networking.networkmanager.enable = true;

  powerManagement.enable = true;

  #hardware.bluetooth.enable = true;

  hardware.opengl.extraPackages = [ pkgs.vaapiIntel ];

}

