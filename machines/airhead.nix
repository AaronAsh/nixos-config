{ config, pkgs, ... }:

{
  imports =
    [
      # ../profiles/android.nix
      ../profiles/audio.nix
      ../profiles/bluetooth.nix
      ../profiles/clitools.nix
      ../profiles/desktop.nix
      # ../profiles/development.nix
      #../profiles/docker.nix
      # ../profiles/dotnet.nix
      # ../profiles/drawing.nix
      ../profiles/email.nix
      # ../profiles/games.nix
      # ../profiles/graphics.nix
      # ../profiles/homeautomation.nix
      # ../profiles/latex.nix
      # ../profiles/livecoding.nix
      # ../profiles/maclaptop.nix
      ../profiles/mathematics.nix
      ../profiles/networking.nix
      #../profiles/powermanagement.nix
      # ../profiles/printing.nix
      ../services/redshift.nix
      ../services/tlp.nix
      ../services/caps2esc.nix
      # ../profiles/rust.nix
      # ../profiles/ruby.nix
      ../profiles/steam.nix
      ../profiles/virtualization-common.nix
      ../profiles/virtualization-libvirtd.nix
      #../profiles/virtualization-virtualbox.nix

      # Experimental
      # ../containers/watchlists.nix
    ];

  nixpkgs.config.allowUnfree = true;

#  networking.firewall.enable = false;
#  networking.firewall.allowPing = true;

  # Speed up development at the cost of possible build race conditions
  nix.buildCores = 2;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Use the GRUB 2 boot loader.
  # boot.loader.grub.enable = true;
  # boot.loader.grub.version = 2;
  # boot.loader.grub.devices = [ "/dev/sda" ];

  # boot.initrd.luks.devices = [
  #   {
  #     name = "root"; device = "/dev/sda2"; preLVM = true;
  #   }
  # ];

  # boot.extraModprobeConfig = ''
  # options resume=/dev/rootvg/swap
  # '';

  boot.kernelPackages = pkgs.linuxPackages_latest;

  networking.hostName = "airhead";
  networking.hostId = "bf6a2b28";

  networking.networkmanager.enable = true;

  hardware.bluetooth.enable = true;

  hardware.opengl.extraPackages = [ pkgs.vaapiIntel ];

}

