{ config, pkgs, ... }:

{
  imports =
    [

      # ../profiles/android.nix
      ../profiles/audio.nix
      ../profiles/bluetooth.nix
      ../profiles/clitools.nix
      ../profiles/desktop.nix
      ../profiles/development.nix
      ../profiles/docker.nix
      # ../profiles/dotnet.nix
      ../profiles/drawing.nix
      ../profiles/email.nix
      ../profiles/games.nix
      #../profiles/dwarf-fortress.nix
      # ../profiles/graphics.nix
      # ../profiles/homeautomation.nix
      # ../profiles/latex.nix
      # ../profiles/livecoding.nix
      # ../profiles/maclaptop.nix
      ../profiles/mathematics.nix
      ../profiles/networking.nix
      #../profiles/powermanagement.nix
      # ../profiles/printing.nix
      # ../profiles/rust.nix
      # ../profiles/ruby.nix
      #../profiles/steam.nix
      ../profiles/virtualization-common.nix
      #../profiles/virtualization-virtualbox.nix
      ../profiles/virtualization-libvirtd.nix

      #../services/redshift.nix
      ../services/caps2esc.nix
      ../services/syncthing.nix
      ../services/nfs.nix

      # Experimental
      # ../containers/watchlists.nix
    ];

  nixpkgs.config.allowUnfree = true;
  #nixpkgs.config.allowBroken = true;

#  networking.firewall.enable = false;
#  networking.firewall.allowPing = true;
# Set a static IP when I didn't want to set up dhcp
#  networking.defaultGateway = "192.168.0.1";
#  networking.interfaces.enp3s0 = {
#    ipAddress = "192.168.0.42";
#    prefixLength = 24;
#  };

  # Speed up development at the cost of possible build race conditions
  nix.buildCores = 12;

  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;

  # Use the GRUB 2 boot loader.
  # boot.loader.grub.enable = true;
  # boot.loader.grub.version = 2;

  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.grub.devices = [ "/dev/sda" ];

  # boot.initrd.luks.devices = [
  #   {
  #     name = "root"; device = "/dev/sda2"; preLVM = true;
  #   }
  # ];

  # boot.extraModprobeConfig = ''
  # options resume=/dev/rootvg/swap
  # '';

  powerManagement = {
    enable = true;
    #cpuFreqGovernor = "schedutil";
    cpuFreqGovernor = "ondemand";  # Here for the Ryzen desktop
  };

  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot = {
    kernelParams = [  "consoleblank=0" "nordrand" ];
  };

  networking.hostName = "megatron";
  networking.hostId = "c60dc069";

  networking.networkmanager.enable = true;

  #hardware.bluetooth.enable = true;

  # hardware.opengl.extraPackages = [ pkgs.vaapiIntel ];

}

