{ config, pkgs, ... }:

{
  imports =
    [
      # ../profiles/android.nix
      ../profiles/audio.nix
      ../profiles/clitools.nix
      ../profiles/desktop.nix
      #../profiles/development.nix
      #../profiles/docker.nix
      # ../profiles/dotnet.nix
      #../profiles/drawing.nix
      #../profiles/email.nix
      #../profiles/games.nix
      # ../profiles/graphics.nix
      # ../profiles/homeautomation.nix
      # ../profiles/latex.nix
      # ../profiles/livecoding.nix
      # ../profiles/maclaptop.nix
      ../profiles/mathematics.nix
      ../profiles/networking.nix
      #../profiles/powermanagement.nix
      # ../profiles/printing.nix
      # ../profiles/rust.nix
      # ../profiles/ruby.nix
      #../profiles/steam.nix
      #../profiles/virtualization-common.nix
      #../profiles/virtualization-virtualbox.nix
      #../profiles/virtualization-libvirtd.nix

      ../services/redshift.nix
      ../services/caps2esc.nix
      #../services/syncthing.nix

      # Experimental
      # ../containers/watchlists.nix
    ];

  nixpkgs.config.allowUnfree = true;

#  networking.firewall.enable = false;
#  networking.firewall.allowPing = true;
# Set a static IP when I didn't want to set up dhcp
#  networking.defaultGateway = "192.168.0.1";
#  networking.interfaces.enp3s0 = {
#    ipAddress = "192.168.0.42";
#    prefixLength = 24;
#  };

  # Speed up development at the cost of possible build race conditions
  nix.buildCores = 6;

  zramSwap = {
    enable = true;
    memoryPercent = 50;
    numDevices = 6;
    priority = 10;
  };

  # Use the extlinux boot loader. (NixOS wants to enable GRUB by default)
  boot.loader.grub.enable = false;
  # Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;

  powerManagement = {
    enable = true;
  };

  boot.kernelPackages = pkgs.linuxPackages_latest;

  networking.hostName = "rockpro64";
  networking.hostId = "07d5a9f9";

  networking.networkmanager.enable = true;

  #hardware.bluetooth.enable = true;

  #AA TODO: Remove after first boot!!!
  #You can initalize the root password to an empty one with this line: (and of course don't forget to set one once you've rebooted or to lock the account with sudo passwd -l root if you use sudo)
  # users.users.root.initialHashedPassword = "";

}

